package com.dika.home.data.remote.datasources

import com.dika.core.utils.Result
import com.dika.home.data.remote.services.PhotoService
import com.dika.home.utils.MockResponseFileReader
import com.google.common.truth.Truth.assertThat
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@ExperimentalCoroutinesApi
class PhotoRemoteDataSourceImplTest {
    private val mockWebServer = MockWebServer()

    private val client = OkHttpClient.Builder()
        .connectTimeout(2, TimeUnit.SECONDS)
        .readTimeout(2, TimeUnit.SECONDS)
        .writeTimeout(2, TimeUnit.SECONDS)
        .build()

    private val api = Retrofit.Builder()
        .baseUrl(mockWebServer.url("/"))
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
        .create(PhotoService::class.java)

    private val dataSource = PhotoRemoteDataSourceImpl(api)

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun `read json file successfully`() {
        val reader = MockResponseFileReader("photos_response.json")
        print(reader.content)
        assertThat(reader.content).isNotNull()
    }

    @Test
    fun `should fetch photos with 200 response`() {
        val reader = MockResponseFileReader("photos_response.json")
        val mockResponse = MockResponse()
            .setResponseCode(200)
            .setBody(reader.content)
        mockWebServer.enqueue(mockResponse)

        val page = 1
        val limit = 2

        runBlocking {
            val actual = dataSource.fetch(limit, page)
            print(actual)
            assertThat(actual).isNotNull()
            assertThat(actual.status).isEqualTo(Result.Status.SUCCESS)
            assertThat(actual.data).isNotNull()
            assertThat(actual.data?.page).isEqualTo(page)
            assertThat(actual.data?.perPage).isEqualTo(limit)
        }
    }
}