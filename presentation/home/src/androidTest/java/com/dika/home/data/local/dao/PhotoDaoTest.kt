package com.dika.home.data.local.dao

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.dika.home.data.local.HomeDatabase
import com.dika.home.data.local.entities.PhotoEntity
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class PhotoDaoTest {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: HomeDatabase
    private lateinit var dao: PhotoDao

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            HomeDatabase::class.java
        ).allowMainThreadQueries().build()
        dao = database.photoDao()
    }

    @After
    fun tearDown() {
        database.close()
    }

    @Test
    fun insertPhoto() = runBlockingTest {
        val photoEntity = PhotoEntity(photoId = 1, photographer = "user1")
        dao.insert(photoEntity)

        val photos = dao.getById(1)
        assertThat(photos).isNotNull()
        assertThat(photos?.photoId).isEqualTo(photoEntity.photoId)
    }

    @Test
    fun deletePhoto() = runBlockingTest {
        dao.deleteAll()
        val photos = dao.getById(1)
        assertThat(photos).isNull()
    }
}