package com.dika.home.data.local.dao

import androidx.paging.PagingSource
import androidx.room.*
import com.dika.core.utils.BaseDao
import com.dika.home.data.local.entities.PhotoEntity
import com.dika.home.data.local.entities.PhotoRemoteKey

@Dao
interface PhotoDao : BaseDao<PhotoEntity> {
    @Query("SELECT * FROM photo")
    fun getAllPaging(): PagingSource<Int, PhotoEntity>

    @Query("SELECT * FROM photo WHERE photoId=:photoId")
    fun getById(photoId: Int): PhotoEntity?

    @Query("DELETE from photo")
    suspend fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertKey(key: PhotoRemoteKey)

    @Update
    fun updateKey(key: PhotoRemoteKey)

    @Query("SELECT * FROM photo_remote_key WHERE photoId = :photoId")
    fun remoteKeyById(photoId: Int): PhotoRemoteKey?

    @Query("SELECT * FROM photo_remote_key ORDER BY id DESC LIMIT 1")
    fun getLastKey(): PhotoRemoteKey?

    @Query("DELETE FROM photo_remote_key")
    suspend fun deleteKeys()

    @Transaction
    suspend fun insertOrUpdate(data: List<PhotoEntity>) {
        data.forEach { it ->
            val photo = getById(it.photoId)
            photo?.let {
                update(it.apply {
                    id = photo.id
                })
            } ?: run {
                insert(it)
            }
        }
    }

    @Transaction
    suspend fun insertOrUpdateKeys(data: List<PhotoRemoteKey>) {
        data.forEach { it ->
            val key = remoteKeyById(it.photoId)
            key?.let {
                updateKey(it.apply {
                    id = key.id
                })
            } ?: run {
                insertKey(it)
            }
        }
    }
}