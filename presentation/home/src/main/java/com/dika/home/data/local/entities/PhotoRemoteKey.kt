package com.dika.home.data.local.entities

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    indices = [Index(value = ["photoId"], unique = true)],
    tableName = "photo_remote_key"
)
data class PhotoRemoteKey(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    val photoId: Int,
    val prevKey: Int?,
    val nextKey: Int?,
)
