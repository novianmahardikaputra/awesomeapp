package com.dika.home.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import androidx.paging.ExperimentalPagingApi
import com.dika.common.extensions.showSnackBar
import com.dika.core.utils.Result
import com.dika.home.R
import com.dika.home.databinding.FragmentDetailBinding
import com.google.android.material.appbar.MaterialToolbar
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class DetailFragment : Fragment() {
    private lateinit var binding: FragmentDetailBinding
    private val viewModel by viewModels<DetailViewModel>()
    private val args by navArgs<DetailFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_detail,
            container,
            false
        )
        binding.lifecycleOwner = this
        return binding.root
    }

    @ExperimentalPagingApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initNavigation()

        viewModel.photoResult.observe(viewLifecycleOwner) {
            when (it.status) {
                Result.Status.SUCCESS -> binding.model = it.data
                Result.Status.ERROR -> showSnackBar(it.message)
                Result.Status.LOADING -> {
                }
            }
        }
    }

    private fun initNavigation() {
        val navController = findNavController()
        view?.findViewById<MaterialToolbar>(R.id.toolbar)?.apply {
            setupWithNavController(
                navController,
                AppBarConfiguration(navController.graph)
            )
        }
    }
}