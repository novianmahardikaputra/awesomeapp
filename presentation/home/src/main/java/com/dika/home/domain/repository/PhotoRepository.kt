package com.dika.home.domain.repository

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import com.dika.core.utils.Result
import com.dika.home.domain.model.Photo
import kotlinx.coroutines.flow.Flow

interface PhotoRepository {
    fun getPhotos(): Flow<PagingData<Photo>>
    fun getPhoto(photoId: Int): LiveData<Result<Photo>>
}