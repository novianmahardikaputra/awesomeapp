package com.dika.home.data.mapper

import com.dika.home.data.local.entities.PhotoEntity
import com.dika.home.data.remote.response.PhotoResponse
import com.dika.home.domain.model.Photo

fun PhotoResponse.toEntity() = PhotoEntity(
    photoId = id,
    width = width,
    height = height,
    url = url,
    photographer = photographer,
    photographerUrl = photographerUrl,
    photographerId = photographerId,
    avgColor = avgColor,
    src = src.toEntity(),
    liked = liked
)

fun PhotoEntity.toModel() = Photo(
    id = id,
    photoId = photoId,
    width = width,
    height = height,
    url = url,
    photographer = photographer,
    photographerUrl = photographerUrl,
    photographerId = photographerId,
    avgColor = avgColor,
    src = src.toModel(),
    liked = liked
)

fun Photo.toEntity() = PhotoEntity(
    id = id,
    photoId = photoId,
    width = width,
    height = height,
    url = url,
    photographer = photographer,
    photographerUrl = photographerUrl,
    photographerId = photographerId,
    avgColor = avgColor,
    src = src.toEntity(),
    liked = liked
)