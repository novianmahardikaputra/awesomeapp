package com.dika.home.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.ExperimentalPagingApi
import androidx.paging.cachedIn
import com.dika.home.domain.repository.PhotoRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    repository: PhotoRepository
) : ViewModel() {

    @ExperimentalPagingApi
    val photoResult = repository.getPhotos().cachedIn(viewModelScope)
}