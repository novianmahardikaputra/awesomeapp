package com.dika.home.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.paging.*
import com.dika.core.utils.Result
import com.dika.home.data.local.HomeDatabase
import com.dika.home.data.mapper.toModel
import com.dika.home.data.paging.PhotoRemoteMediator
import com.dika.home.data.remote.datasources.PhotoRemoteDataSource
import com.dika.home.domain.model.Photo
import com.dika.home.domain.repository.PhotoRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class PhotoRepositoryImpl @Inject constructor(
    private val database: HomeDatabase,
    private val remoteDataSource: PhotoRemoteDataSource
) : PhotoRepository {

    @ExperimentalPagingApi
    override fun getPhotos(): Flow<PagingData<Photo>> = Pager(
        config = PagingConfig(pageSize = 20),
        remoteMediator = PhotoRemoteMediator(database, remoteDataSource)
    ) {
        database.photoDao().getAllPaging()
    }.flow.distinctUntilChanged().map { pagingData ->
        pagingData.map { it.toModel() }
    }

    override fun getPhoto(photoId: Int): LiveData<Result<Photo>> = liveData(Dispatchers.IO) {
        database.photoDao().getById(photoId)?.let {
            emit(Result.success(it.toModel()))
        } ?: run {
            emit(Result.error("Data not found", null))
        }
    }
}