package com.dika.home.di

import com.dika.home.data.remote.datasources.PhotoRemoteDataSource
import com.dika.home.data.remote.datasources.PhotoRemoteDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
abstract class RemoteDataSourceModules {
    @Binds
    @Singleton
    abstract fun bindPhotoRemoteDataSource(photoRemoteDataSourceImpl: PhotoRemoteDataSourceImpl): PhotoRemoteDataSource
}