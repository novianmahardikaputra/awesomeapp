package com.dika.home.data.remote.datasources

import com.dika.core.utils.Result
import com.dika.home.data.remote.response.PhotosResponse

interface PhotoRemoteDataSource {
    suspend fun fetch(limit: Int, page: Int): Result<PhotosResponse>
}