package com.dika.home.data.remote.services

import com.dika.home.data.remote.response.PhotosResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface PhotoService {
    @GET("curated")
    suspend fun getCurated(
        @Query("per_page") limit: Int,
        @Query("page") page: Int,
    ): Response<PhotosResponse>
}