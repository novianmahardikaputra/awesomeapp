package com.dika.home.domain.model

data class Photo(
    val id: Int = 0,
    val photoId: Int = 0,
    val width: Int = 0,
    val height: Int = 0,
    val url: String = "",
    val photographer: String = "",
    val photographerUrl: String = "",
    val photographerId: Int = 0,
    val avgColor: String = "",
    val src: PhotoSrc = PhotoSrc(),
    val liked: Boolean = false
)