package com.dika.home.data.local.entities

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    indices = [Index(value = ["photoId"], unique = true)],
    tableName = "photo"
)
data class PhotoEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    val photoId: Int = 0,
    val width: Int = 0,
    val height: Int = 0,
    val url: String = "",
    val photographer: String = "",
    val photographerUrl: String = "",
    val photographerId: Int = 0,
    val avgColor: String = "",
    @Embedded(prefix = "src_")
    val src: PhotoSrcEntity = PhotoSrcEntity(),
    val liked: Boolean = false
)