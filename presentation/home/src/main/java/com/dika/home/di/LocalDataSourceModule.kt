package com.dika.home.di

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.dika.home.data.local.HomeDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import timber.log.Timber
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object LocalDataSourceModule {
    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext context: Context): HomeDatabase {
        return Room.databaseBuilder(context, HomeDatabase::class.java, HomeDatabase.DATABASE_NAME)
            .fallbackToDestructiveMigration()
            .addCallback(object : RoomDatabase.Callback() {
                override fun onDestructiveMigration(db: SupportSQLiteDatabase) {
                    Timber.d("Destructive Migration DB")
                    super.onDestructiveMigration(db)
                    context.deleteDatabase(HomeDatabase.DATABASE_NAME)
                }
            })
            .build()
    }


    @Singleton
    @Provides
    fun providePhotoDao(db: HomeDatabase) = db.photoDao()
}