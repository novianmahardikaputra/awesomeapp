package com.dika.home.data.remote.response

import com.google.gson.annotations.SerializedName

data class PhotoResponse(
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("width")
    val width: Int = 0,
    @SerializedName("height")
    val height: Int = 0,
    @SerializedName("url")
    val url: String = "",
    @SerializedName("photographer")
    val photographer: String = "",
    @SerializedName("photographer_url")
    val photographerUrl: String = "",
    @SerializedName("photographer_id")
    val photographerId: Int = 0,
    @SerializedName("avg_color")
    val avgColor: String = "",
    @SerializedName("src")
    val src: PhotoSrcResponse = PhotoSrcResponse(),
    @SerializedName("liked")
    val liked: Boolean = false
)