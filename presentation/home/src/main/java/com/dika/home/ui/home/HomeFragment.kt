package com.dika.home.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.dika.common.extensions.px
import com.dika.common.extensions.showDivider
import com.dika.common.extensions.showSnackBar
import com.dika.home.R
import com.dika.home.databinding.FragmentHomeBinding
import com.dika.home.domain.model.Photo
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.MaterialToolbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class HomeFragment : Fragment(), HomePhotoAdapter.PhotoAdapterListener,
    PagingLoadStateAdapter.FooterAdapterListener {
    private lateinit var binding: FragmentHomeBinding
    private val viewModel by viewModels<HomeViewModel>()
    private val homePhotoAdapter by lazy { HomePhotoAdapter(this) }
    private val footerPhotoAdapter by lazy { PagingLoadStateAdapter(this) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_home,
            container,
            false
        )
        binding.lifecycleOwner = this
        binding.fragment = this
        return binding.root
    }

    @ExperimentalPagingApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initNavigation()
        initView()
        initAdapter()

        lifecycleScope.launchWhenStarted {
            viewModel.photoResult.collectLatest {
                homePhotoAdapter.submitData(it)
            }
        }
    }

    private fun initNavigation() {
        val navController = findNavController()
        view?.findViewById<MaterialToolbar>(R.id.toolbar)?.apply {
            setupWithNavController(
                navController,
                AppBarConfiguration(navController.graph)
            )
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.actionGrid -> {
                        binding.rvPhoto.apply {
                            (layoutManager as GridLayoutManager).spanCount = 2
                            showDivider(16.px, 16.px)
                            setPadding(16.px, 16.px, 0, 16.px)
                        }
                        homePhotoAdapter.setListView(false)
                        footerPhotoAdapter.setListView(false)
                    }
                    R.id.actionList -> {
                        binding.rvPhoto.apply {
                            (layoutManager as GridLayoutManager).spanCount = 1
                            showDivider(0, 0)
                            setPadding(0, 0, 0, 0)
                        }
                        homePhotoAdapter.setListView(true)
                        footerPhotoAdapter.setListView(true)
                    }
                }
                return@setOnMenuItemClickListener true
            }
        }
    }

    private fun initAdapter() {
        binding.rvPhoto.apply {
            val gridLayoutManager =
                GridLayoutManager(requireContext(), 2, RecyclerView.VERTICAL, false)
            gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return if (position == homePhotoAdapter.itemCount
                        && footerPhotoAdapter.itemCount > 0
                        && !homePhotoAdapter.isList()
                    ) {
                        2
                    } else {
                        1
                    }
                }
            }

            layoutManager = gridLayoutManager
            showDivider(16.px, 16.px)
            setPadding(16.px, 16.px, 0, 16.px)
            adapter = homePhotoAdapter.withLoadStateFooter(footerPhotoAdapter)
        }

        lifecycleScope.launch {
            homePhotoAdapter.loadStateFlow.collectLatest { it ->
                when (it.refresh) {
                    is LoadState.NotLoading -> binding.showLoading = false
                    LoadState.Loading -> {
                    }
                    is LoadState.Error -> {
                        binding.showLoading = false
                        val message = (it as? LoadState.Error)?.error?.message
                        message?.let {
                            showSnackBar(it)
                        }
                    }
                }
            }
        }
    }

    private fun initView() {
        binding.appBar.addOnOffsetChangedListener(appBarOffsetListener)
        binding.imCover.load("https://images.pexels.com/photos/9321606/pexels-photo-9321606.jpeg?auto=compress&cs=tinysrgb&h=650&w=940")
    }

    private val appBarOffsetListener = AppBarLayout.OnOffsetChangedListener { _, verticalOffset ->
        binding.appBar.post {
            if (verticalOffset == 0) { //expanded
                setTitle("")
            } else {
                if (view != null) {
                    findNavController().currentDestination?.label?.let { setTitle(it.toString()) }
                }
            }
        }
    }

    private fun setTitle(title: String) {
        view?.findViewById<MaterialToolbar>(R.id.toolbar)?.title = title
    }

    override fun onItemClick(model: Photo) {
        val args = Bundle()
        args.putInt("photoId", model.photoId)
        val deeplink = findNavController()
            .createDeepLink()
            .setDestination(R.id.detailFragment)
            .setArguments(args)
            .createPendingIntent()
        deeplink.send()
    }

    fun onRefresh() {
        homePhotoAdapter.refresh()
    }

    override fun onDestroyView() {
        binding.appBar.removeOnOffsetChangedListener(appBarOffsetListener)
        super.onDestroyView()
    }

    override fun onRetry() {
        homePhotoAdapter.retry()
    }
}