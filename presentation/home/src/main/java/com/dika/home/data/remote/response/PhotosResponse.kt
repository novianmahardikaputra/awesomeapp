package com.dika.home.data.remote.response

import com.google.gson.annotations.SerializedName

data class PhotosResponse(
    @SerializedName("page")
    val page: Int = 0,
    @SerializedName("per_page")
    val perPage: Int = 0,
    @SerializedName("photos")
    val photos: List<PhotoResponse> = listOf(),
    @SerializedName("total_results")
    val totalResults: Int = 0,
    @SerializedName("next_page")
    val nextPage: String = "",
    @SerializedName("prev_page")
    val prevPage: String = ""
)