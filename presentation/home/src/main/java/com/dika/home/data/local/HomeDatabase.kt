package com.dika.home.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.dika.home.data.local.dao.PhotoDao
import com.dika.home.data.local.entities.PhotoEntity
import com.dika.home.data.local.entities.PhotoRemoteKey

@Database(
    entities = [PhotoEntity::class, PhotoRemoteKey::class],
    version = 1,
    exportSchema = false
)
abstract class HomeDatabase : RoomDatabase() {
    abstract fun photoDao(): PhotoDao

    companion object {
        const val DATABASE_NAME = "home_db"
    }
}