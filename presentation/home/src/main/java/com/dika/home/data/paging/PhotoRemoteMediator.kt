package com.dika.home.data.paging

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.dika.core.utils.Result
import com.dika.home.data.local.HomeDatabase
import com.dika.home.data.local.entities.PhotoEntity
import com.dika.home.data.local.entities.PhotoRemoteKey
import com.dika.home.data.mapper.toEntity
import com.dika.home.data.remote.datasources.PhotoRemoteDataSource
import retrofit2.HttpException
import timber.log.Timber
import java.io.IOException

@ExperimentalPagingApi
class PhotoRemoteMediator(
    private val database: HomeDatabase,
    private val remoteDataSource: PhotoRemoteDataSource
) : RemoteMediator<Int, PhotoEntity>() {
    private val startingIndex = 1
    private val photoDao = database.photoDao()

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, PhotoEntity>
    ): MediatorResult {
        return try {
            val loadKey = when (loadType) {
                LoadType.REFRESH -> startingIndex
                LoadType.PREPEND -> return MediatorResult.Success(endOfPaginationReached = true)
                LoadType.APPEND -> {
                    val remoteKey = database.withTransaction {
                        photoDao.getLastKey()
                    }
                    Timber.d(remoteKey.toString())
                    remoteKey?.nextKey
                        ?: return MediatorResult.Success(endOfPaginationReached = true)
                }
            }

            Timber.d(loadKey.toString())

            val result = remoteDataSource.fetch(state.config.pageSize, loadKey)
            when (result.status) {
                Result.Status.SUCCESS -> {
                    val endPagination = result.data?.photos?.isEmpty() == true
                    result.data?.photos?.map {
                        it.toEntity()
                    }?.let { it ->
                        val prevKey = if (loadKey == startingIndex) null else loadKey - 1
                        val nextKey = if (endPagination) null else loadKey + 1
                        Timber.d("Prev key: $prevKey")
                        Timber.d("Next key: $nextKey")
                        val remoteKeys = it.map {
                            PhotoRemoteKey(
                                photoId = it.photoId,
                                prevKey = prevKey,
                                nextKey = nextKey
                            )
                        }
                        database.withTransaction {
                            photoDao.apply {
                                if (loadType == LoadType.REFRESH) {
                                    deleteAll()
                                    deleteKeys()
                                }
                                insertOrUpdate(it)
                                insertOrUpdateKeys(remoteKeys)
                            }
                        }
                    }

                    MediatorResult.Success(
                        endOfPaginationReached = endPagination
                    )
                }
                Result.Status.ERROR -> throw IllegalArgumentException(result.message)
                Result.Status.LOADING -> MediatorResult.Success(false)
            }
        } catch (e: IOException) {
            Timber.e(e)
            MediatorResult.Error(e)
        } catch (e: HttpException) {
            Timber.e(e)
            MediatorResult.Error(e)
        } catch (e: Exception) {
            Timber.e(e)
            MediatorResult.Error(e)
        }
    }
}