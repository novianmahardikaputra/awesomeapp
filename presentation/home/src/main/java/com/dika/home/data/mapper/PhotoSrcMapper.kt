package com.dika.home.data.mapper

import com.dika.home.data.local.entities.PhotoSrcEntity
import com.dika.home.data.remote.response.PhotoSrcResponse
import com.dika.home.domain.model.PhotoSrc

fun PhotoSrcResponse.toEntity() = PhotoSrcEntity(
    original = original,
    large2x = large2x,
    large = large,
    medium = medium,
    small = small,
    portrait = portrait,
    landscape = landscape,
    tiny = tiny
)

fun PhotoSrcEntity.toModel() = PhotoSrc(
    original = original,
    large2x = large2x,
    large = large,
    medium = medium,
    small = small,
    portrait = portrait,
    landscape = landscape,
    tiny = tiny
)

fun PhotoSrc.toEntity() = PhotoSrcEntity(
    original = original,
    large2x = large2x,
    large = large,
    medium = medium,
    small = small,
    portrait = portrait,
    landscape = landscape,
    tiny = tiny
)