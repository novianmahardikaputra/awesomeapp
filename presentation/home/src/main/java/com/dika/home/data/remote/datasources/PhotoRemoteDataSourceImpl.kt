package com.dika.home.data.remote.datasources

import com.dika.core.utils.BaseDataSource
import com.dika.home.data.remote.services.PhotoService
import javax.inject.Inject

class PhotoRemoteDataSourceImpl @Inject constructor(
    private val photoService: PhotoService
) : BaseDataSource(), PhotoRemoteDataSource {
    override suspend fun fetch(limit: Int, page: Int) =
        getResult { photoService.getCurated(limit, page) }
}