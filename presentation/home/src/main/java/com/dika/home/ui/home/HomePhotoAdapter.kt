package com.dika.home.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.dika.home.databinding.ItemPhotoGridBinding
import com.dika.home.databinding.ItemPhotoListBinding
import com.dika.home.domain.model.Photo

class HomePhotoAdapter(
    private val listener: PhotoAdapterListener
) : PagingDataAdapter<Photo, RecyclerView.ViewHolder>(Companion) {
    private var listView = false

    enum class ViewType {
        GRID,
        LIST
    }

    interface PhotoAdapterListener {
        fun onItemClick(model: Photo)
    }

    companion object : DiffUtil.ItemCallback<Photo>() {
        override fun areItemsTheSame(oldItem: Photo, newItem: Photo): Boolean =
            oldItem === newItem

        override fun areContentsTheSame(
            oldItem: Photo,
            newItem: Photo
        ): Boolean =
            oldItem.id == newItem.id
    }

    fun setListView(list: Boolean) {
        this.listView = list
        notifyItemRangeChanged(0, itemCount)
    }

    fun isList(): Boolean = listView

    class PhotoListViewHolder(
        private val context: Context,
        private val binding: ItemPhotoListBinding,
        listener: PhotoAdapterListener
    ) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.listener = listener
        }

        fun onBind(item: Photo) {
            binding.model = item
            binding.imPhoto.load(item.src.small)
        }
    }

    class PhotoGridViewHolder(
        private val context: Context,
        private val binding: ItemPhotoGridBinding,
        listener: PhotoAdapterListener
    ) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.listener = listener
        }

        fun onBind(item: Photo) {
            binding.model = item
            binding.imPhoto.load(item.src.small)
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (listView) {
            return ViewType.LIST.ordinal
        }
        return ViewType.GRID.ordinal
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            ViewType.LIST.ordinal -> PhotoListViewHolder(
                parent.context,
                ItemPhotoListBinding.inflate(layoutInflater, parent, false),
                listener
            )
            else -> PhotoGridViewHolder(
                parent.context,
                ItemPhotoGridBinding.inflate(layoutInflater, parent, false),
                listener
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        item?.let {
            when (holder) {
                is PhotoListViewHolder -> holder.onBind(it)
                is PhotoGridViewHolder -> holder.onBind(it)
            }
        }
    }
}