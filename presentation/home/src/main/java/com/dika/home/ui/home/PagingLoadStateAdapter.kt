package com.dika.home.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.dika.home.databinding.PlaceholderItemPhotoBinding
import retrofit2.HttpException
import java.io.IOException

class PagingLoadStateAdapter(private val listener: FooterAdapterListener) :
    LoadStateAdapter<PagingLoadStateAdapter.FooterViewHolder>() {
    private var listView = false

    interface FooterAdapterListener {
        fun onRetry()
    }

    fun setListView(list: Boolean) {
        this.listView = list
        notifyItemRangeChanged(0, itemCount)
    }

    fun isList(): Boolean = listView

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): FooterViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return FooterViewHolder(
            PlaceholderItemPhotoBinding.inflate(
                layoutInflater,
                parent,
                false
            ), listener
        )
    }

    override fun onBindViewHolder(holder: FooterViewHolder, loadState: LoadState) {
        holder.onBind(loadState, listView)
    }

    class FooterViewHolder(
        private val binding: PlaceholderItemPhotoBinding,
        listener: FooterAdapterListener,
    ) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.listener = listener
        }

        fun onBind(state: LoadState, listView: Boolean) {
            when (state) {
                is LoadState.NotLoading -> {
                }
                LoadState.Loading -> {
                    binding.showRetry = false
                    if (listView) {
                        binding.showGridLoading = false
                        binding.showListLoading = true
                    } else {
                        binding.showListLoading = false
                        binding.showGridLoading = true
                    }
                }
                is LoadState.Error -> {
                    binding.showRetry = true
                    val message = when (state.error) {
                        is HttpException -> "No Internet Connection"
                        is IOException -> "No Internet Connection"
                        else -> state.error.message
                    }
                    binding.errorMessage = message
                }
            }
        }
    }
}