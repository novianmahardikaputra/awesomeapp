package com.dika.core.utils

import com.dika.core.constants.Network
import retrofit2.HttpException
import retrofit2.Response
import timber.log.Timber
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

abstract class BaseDataSource {
    protected suspend fun <T> getResult(call: suspend () -> Response<T>): Result<T> {
        try {
            val response = call()
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null) return Result.success(body)
            }
            return Result.error("${response.code()} ${response.message()}")
        } catch (e: Exception) {
            Timber.e(e)
            return when (e) {
                is ConnectException -> {
                    Result.error(Network.CONNECT_EXCEPTION)
                }
                is UnknownHostException -> {
                    Result.error(Network.UNKNOWN_HOST_EXCEPTION)
                }
                is SocketTimeoutException -> {
                    Result.error(Network.SOCKET_TIME_OUT_EXCEPTION)
                }
                is HttpException -> {
                    Result.error(Network.UNKNOWN_NETWORK_EXCEPTION)
                }
                else -> {
                    Result.error(Network.UNKNOWN_NETWORK_EXCEPTION)
                }
            }
        }
    }
}
