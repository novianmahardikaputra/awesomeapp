package com.dika.core.utils

import com.dika.core.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

class TokenInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val newRequest = chain.request().newBuilder()
            .addHeader("Authorization", BuildConfig.API_KEY)
            .addHeader("Accept", "application/json")
            .build()
        return chain.proceed(newRequest)
    }
}