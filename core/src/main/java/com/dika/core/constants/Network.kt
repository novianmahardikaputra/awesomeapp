package com.dika.core.constants

object Network {
    const val SOCKET_TIME_OUT_EXCEPTION =
        "Timeout Request. Please check your internet connection"
    const val UNKNOWN_NETWORK_EXCEPTION =
        "Unknown network problem. Please check your internet connection"
    const val CONNECT_EXCEPTION =
        "Server is unavailable. Please check your internet connection"
    const val UNKNOWN_HOST_EXCEPTION =
        "Server is unavailable. Please check your internet connection"
}