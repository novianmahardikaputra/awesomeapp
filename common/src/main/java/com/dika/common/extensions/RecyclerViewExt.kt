package com.dika.common.extensions

import android.content.res.ColorStateList
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.LayerDrawable
import android.os.Build
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView

fun createDivider(size: Int, color: Int? = null): LayerDrawable {
    val drawable = GradientDrawable()
    color?.let {
        drawable.color = ColorStateList.valueOf(it)
    }
    return LayerDrawable(arrayOf(drawable)).also {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            it.setLayerSize(0, size, size)
        }
    }
}

fun RecyclerView.showDivider(vertical: Int, horizontal: Int, color: Int? = null) {
    val verticalDecoration = DividerItemDecoration(context, RecyclerView.VERTICAL).apply {
        setDrawable(createDivider(vertical, color))
    }
    val horizontalDecoration = DividerItemDecoration(context, RecyclerView.HORIZONTAL).apply {
        setDrawable(createDivider(horizontal, color))
    }
    clearDecorations()
    addItemDecoration(verticalDecoration)
    addItemDecoration(horizontalDecoration)
}

fun RecyclerView.clearDecorations() {
    if (itemDecorationCount > 0) {
        for (i in itemDecorationCount - 1 downTo 0) {
            removeItemDecorationAt(i)
        }
    }
}
