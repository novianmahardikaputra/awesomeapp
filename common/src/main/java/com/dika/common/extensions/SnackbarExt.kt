package com.dika.common.extensions

import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar

fun Fragment.showSnackBar(
    message: String?,
    action: String? = null,
    callback: (() -> Unit)? = null
): Snackbar {
    val m = message ?: ""
    val snack = Snackbar.make(
        requireView(),
        m,
        if (action != null) Snackbar.LENGTH_INDEFINITE else Snackbar.LENGTH_SHORT
    ).apply {
        action?.let {
            setAction(it) {
                dismiss()
                callback?.invoke()
            }
        }
    }
    snack.show()
    return snack
}
