package com.dika.common.extensions

import android.view.View

fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide() {
    visibility = View.GONE
}

fun View.shown(): Boolean {
    return visibility == View.VISIBLE
}