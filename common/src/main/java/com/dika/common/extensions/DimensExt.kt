package com.dika.common.extensions

import android.content.res.Resources

val Int.dp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()
val Int.px: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()
val Number.sp: Float
    get() = (this.toFloat() / Resources.getSystem().displayMetrics.scaledDensity)