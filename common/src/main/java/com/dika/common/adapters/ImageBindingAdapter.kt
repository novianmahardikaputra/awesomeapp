package com.dika.common.adapters

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import coil.load

@BindingAdapter("app:showUrl")
fun ImageView.showUrl(url: String?) {
    load(url)
}
