package com.dika.common.adapters

import androidx.annotation.ColorRes
import androidx.annotation.Dimension
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.dika.common.extensions.showDivider

@BindingAdapter("app:setAdapter")
fun RecyclerView.bindRecyclerViewAdapter(adapter: RecyclerView.Adapter<*>) {
    this.run {
        this.adapter = adapter
    }
}

@BindingAdapter(
    "app:dividerVertical",
    "app:dividerHorizontal",
    "app:dividerColor",
    requireAll = false
)
fun RecyclerView.setDivider(
    @Dimension vertical: Float?,
    @Dimension horizontal: Float?,
    @ColorRes color: Int?
) {
    showDivider((vertical ?: 0).toInt(), (horizontal ?: 0).toInt(), color)
}