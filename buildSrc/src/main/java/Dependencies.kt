object Modules {
    val app = ":app"
    val common = ":common"
    val core = ":core"

    // Features
    val home = ":presentation:home"
    val detail = ":presentation:detail"
}

object AppConfig {
    val aplicationId = "com.dika.awesomeapp"
    val minSdk = 21
    val targetSdk = 30
    val compileSdkVersion = 30
    val versionCode = 1
    val databaseVersion = 1
    val versionName = "1.0.0"
    val buildToolsVersion = "30.0.3"
}

object Version {
    val kotlin = "1.5.21"
    val safeArgs = "2.1.0-alpha02"
    val gradle = "4.2.2"
}

object AndroidLibrary {
    //KOTLIN
    val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Version.kotlin}"
    val coroutineCore =
        "org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.3"
    val coroutineAndroid =
        "org.jetbrains.kotlinx:kotlinx-coroutines-android:1.4.3"

    //THEME
    val core = "androidx.core:core-ktx:1.6.0"
    val appCompat = "androidx.appcompat:appcompat:1.3.0"
    val material = "com.google.android.material:material:1.4.0"
    val fragment = "androidx.activity:activity-ktx:1.3.0-rc01"
    val activity = "androidx.activity:activity-ktx:1.3.0-rc01"
    val multidex = "androidx.multidex:multidex:2.0.1"
    val constraint = "androidx.constraintlayout:constraintlayout:2.0.4"
    val swipeRefresh = "androidx.swiperefreshlayout:swiperefreshlayout:1.1.0"

    //NAVIGATION
    val navigationFragment = "androidx.navigation:navigation-fragment-ktx:2.3.5"
    val navigationUI = "androidx.navigation:navigation-ui-ktx:2.3.5"

    //HILT
    val hilt = "com.google.dagger:hilt-android:2.38"
    val hiltLifecycle = "androidx.hilt:hilt-lifecycle-viewmodel:1.0.0-alpha03"
    val hiltAndroidCompiler = "com.google.dagger:hilt-android-compiler:2.38"
    val hiltCompiler = "androidx.hilt:hilt-compiler:1.0.0"
    val hiltCommon = "androidx.hilt:hilt-common:1.0.0-alpha02"
    val hiltTest = "com.google.dagger:hilt-android-testing:2.38.1"
    val hiltKaptTest = "com.google.dagger:hilt-android-compiler:2.38.1"
    val hiltAndroidTest = "com.google.dagger:hilt-android-testing:2.38.1"
    val hiltKaptAndroidTest = "com.google.dagger:hilt-android-compiler:2.38.1"

    //NETWORK
    val retrofit = "com.squareup.retrofit2:retrofit:2.9.0"
    val retrofitCoroutineAdapter =
        "com.jakewharton.retrofit:retrofit2-kotlin-coroutines-adapter:0.9.2"
    val retrofitGsonConverter = "com.squareup.retrofit2:converter-gson:2.9.0"
    val okHttpLogging = "com.squareup.okhttp3:logging-interceptor:4.9.1"

    //LIFECYCLE
    val lifecycleExtension = "androidx.lifecycle:lifecycle-extensions:2.2.0"
    val lifecycleViewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:2.3.1"
    val lifecycleLiveData = "androidx.lifecycle:lifecycle-livedata-ktx:2.3.1"
    val lifecycleRuntime = "androidx.lifecycle:lifecycle-runtime-ktx:2.3.1"
    val lifecycleCommon = "androidx.lifecycle:lifecycle-common-java8:2.3.1"
    val lifecycleSavedState =
        "androidx.lifecycle:lifecycle-viewmodel-savedstate:2.4.0-alpha01"

    //PAGING
    val paging = "androidx.paging:paging-runtime-ktx:3.0.0"

    //STORAGE
    val room = "androidx.room:room-ktx:2.4.0-alpha02"
    val roomRuntime = "androidx.room:room-runtime:2.4.0-alpha02"
    val roomCompiler = "androidx.room:room-compiler:2.4.0-alpha02"

    //LOGGING
    val timber = "com.jakewharton.timber:timber:4.7.1"

    //IMAGE
    val coil = "io.coil-kt:coil:1.1.0"

    //LOADING
    val facebookShimmer = "com.facebook.shimmer:shimmer:0.5.0"

    //TEST
    val espresso = "androidx.test.espresso:espresso-core:3.4.0"
    val androidJunit = "androidx.test.ext:junit:1.1.3"
    val runner = "androidx.test:runner:1.4.0"
    val junit = "junit:junit:4.13.2"
    val robolectric = "org.robolectric:robolectric:4.4"
    val googleTruth = "com.google.truth:truth:1.1.3"
    val mockWebServer = "com.squareup.okhttp3:mockwebserver:4.9.1"
    val coreTesting = "androidx.arch.core:core-testing:2.1.0"
    val coroutineTest = "org.jetbrains.kotlinx:kotlinx-coroutines-test:1.4.2"
}

object Libraries {
    val coreLibraries = arrayListOf<String>().apply {
        add(AndroidLibrary.kotlin)
        add(AndroidLibrary.core)
        add(AndroidLibrary.multidex)
        add(AndroidLibrary.timber)
    }

    val networkLibraries = arrayListOf<String>().apply {
        add(AndroidLibrary.retrofit)
        add(AndroidLibrary.retrofitGsonConverter)
        add(AndroidLibrary.retrofitCoroutineAdapter)
        add(AndroidLibrary.okHttpLogging)
    }

    val coroutinesLibraries = arrayListOf<String>().apply {
        add(AndroidLibrary.coroutineCore)
        add(AndroidLibrary.coroutineAndroid)
    }

    val lifecycleLibraries = arrayListOf<String>().apply {
        add(AndroidLibrary.lifecycleViewModel)
        add(AndroidLibrary.lifecycleSavedState)
        add(AndroidLibrary.lifecycleRuntime)
        add(AndroidLibrary.lifecycleLiveData)
        add(AndroidLibrary.lifecycleExtension)
        add(AndroidLibrary.lifecycleCommon)
    }

    val themeLibraries = arrayListOf<String>().apply {
        add(AndroidLibrary.material)
        add(AndroidLibrary.appCompat)
        add(AndroidLibrary.constraint)
        add(AndroidLibrary.fragment)
        add(AndroidLibrary.activity)
        add(AndroidLibrary.swipeRefresh)
        add(AndroidLibrary.facebookShimmer)
    }

    val navigationLibraries = arrayListOf<String>().apply {
        add(AndroidLibrary.navigationFragment)
        add(AndroidLibrary.navigationUI)
    }

    val hiltLibraries = arrayListOf<String>().apply {
        add(AndroidLibrary.hilt)
        add(AndroidLibrary.hiltLifecycle)
        add(AndroidLibrary.hiltCommon)
    }

    val hiltKaptLibraries = arrayListOf<String>().apply {
        add(AndroidLibrary.hiltAndroidCompiler)
        add(AndroidLibrary.hiltCompiler)
    }

    val storageLibrary = arrayListOf<String>().apply {
        add(AndroidLibrary.room)
        add(AndroidLibrary.roomRuntime)
    }

    val storageKaptLibrary = arrayListOf<String>().apply {
        add(AndroidLibrary.roomCompiler)
    }

    val imageLibraries = arrayListOf<String>().apply {
        add(AndroidLibrary.coil)
    }

    val androidTestLibraries = arrayListOf<String>().apply {
        add(AndroidLibrary.androidJunit)
        add(AndroidLibrary.runner)
        add(AndroidLibrary.espresso)
        add(AndroidLibrary.hiltAndroidTest)
        add(AndroidLibrary.robolectric)
        add(AndroidLibrary.googleTruth)
        add(AndroidLibrary.mockWebServer)
        add(AndroidLibrary.coreTesting)
        add(AndroidLibrary.coroutineTest)
    }

    val testLibraries = arrayListOf<String>().apply {
        add(AndroidLibrary.junit)
        add(AndroidLibrary.hiltTest)
        add(AndroidLibrary.robolectric)
        add(AndroidLibrary.googleTruth)
        add(AndroidLibrary.mockWebServer)
        add(AndroidLibrary.coreTesting)
        add(AndroidLibrary.coroutineTest)
    }

    val testKaptLibraries = arrayListOf<String>().apply {
        add(AndroidLibrary.hiltKaptTest)
    }

    val androidTestKaptLibraries = arrayListOf<String>().apply {
        add(AndroidLibrary.hiltKaptAndroidTest)
    }
}
